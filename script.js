var inputBoard = [
    [0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0]
];
let numSelected = null;
window.onload = function(){
    setGame();
}

function setGame(){
    for (let i=1; i<10; i++){
        let number = document.createElement("div");
        number.id = i;
        number.innerText = i;
        number.addEventListener("click", selectNumber);
        number.classList.add("number");
        document.getElementById("selectNumbers").appendChild(number);
    }
    for (let r=0; r<9; r++){
        for (let c=0; c<9; c++){
            let cell = document.createElement("div");
            cell.id = r.toString() + "-" + c.toString();
            cell.classList.add("cell");
            cell.classList.add("unsolvedCell");
            if(inputBoard[r][c] != 0){
                cell.innerText = inputBoard[r][c];
            }
            cell.addEventListener("click", fillCell);
            if (r == 2 || r == 5){
                cell.classList.add("bottom");
            }
            if (c == 2 || c == 5){
                cell.classList.add("side");
            }
            document.getElementById("inputBoard").appendChild(cell);
        }
    }
    document.getElementById('solve').onclick = function() {
        solveBoard();
    }
    document.getElementById('reset').onclick = function() {
        resetBoard();
    }
}

function selectNumber(){
    if (numSelected != null){
        numSelected.classList.remove("selected");
    }
    numSelected = this;
    numSelected.classList.add("selected");
}
function fillCell(){
    let currentCell = this;
    if (numSelected != null){
        if (validCell(currentCell, parseInt(numSelected.innerText))){
            currentCell.innerText = numSelected.innerText;
            inputBoard[parseInt(currentCell.id[0])][parseInt(currentCell.id[2])] = parseInt(numSelected.innerText);
        }
    }
}
function validCell(currentCell, n){
    return (validRow(currentCell, n) && validColumn(currentCell, n) && validBox(currentCell, n))
}
function validRow(currentCell, n){
    let rowNumber = parseInt(currentCell.id[0]);
    for (let j=0; j<9; j++){
        if (inputBoard[rowNumber][j] == n && j != parseInt(currentCell.id[2])){
            return false;
        }
    }
    return true;
}
function validColumn(currentCell, n){
    let columnNumber = parseInt(currentCell.id[2]);
    for (let j=0; j<9; j++){
        if (inputBoard[j][columnNumber] == n && j != parseInt(currentCell.id[0])){
            return false;
        }
    }
    return true;
}
function validBox(currentCell, n){
    let boxI = Math.floor(parseInt(currentCell.id[0])/3)*3;
    let boxJ = Math.floor(parseInt(currentCell.id[2])/3)*3;
    for (let i=boxI; i<boxI+3; i++){
        for (let j=boxJ; j<boxJ+3; j++){
            if (inputBoard[i][j] == n && (i != parseInt(currentCell.id[0]) || j != parseInt(currentCell.id[2]))){
                return false;
            }
        }
    }
    return true;
}
function resetBoard(){
    for (let i=0; i<9; i++){
        for (let j=0; j<9; j++){
            document.getElementById(i.toString() + "-" + j.toString()).classList.remove("solvedCell");
            document.getElementById(i.toString() + "-" + j.toString()).classList.add("unsolvedCell");
            inputBoard[i][j] = 0;
            document.getElementById(i.toString() + "-" + j.toString()).innerText = "";
        }
    }
}
let first = true;
function solveBoard(){
    coordinates = findEmptyCell();
    let i = coordinates[0];
    let j = coordinates[1];
    if (i==10 && j==10){
        return true;
    }
    let emptyCell = document.getElementById(i.toString() + "-" + j.toString());
    for (let n=1; n<10; n++){
        if (validCell(emptyCell, n)){
            inputBoard[i][j] = n;
            emptyCell.innerText = n.toString();
            emptyCell.classList.remove("unsolvedCell");
            emptyCell.classList.add("solvedCell");
            first = false;
            if (solveBoard()){
                return solveBoard();
            }
            inputBoard[i][j] = 0;
            emptyCell.innerText = "";
        }
    }
    return false;
}

function findEmptyCell(){
    for (let i=0; i<9; i++){
        for (let j=0; j<9; j++){
            if (inputBoard[i][j] == 0){
                return [i, j];
            }
        }
    }
    return [10, 10];
}